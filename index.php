
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "css/style.min.css" rel = "stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gowun+Batang:wght@700&display=swap" rel="stylesheet">
    <title>Product list</title>

</head>
<body>
    <div class = "topMenu">
        <h1>Product list</h1>
        <div class = "changeList">
            <div>
                <input type = "submit" value = "ADD" class = "addProduct" onclick="document.location='add.php'"></input>
                <input type = "button" value = "MASS DELETE" class = "massDelete" id = "delete-product-btn" form="productsCheck"></input>
            </div>
        </div>
    </div>
    <hr>

    <?php
      require 'modules/Database.php';
      // $book = new Book();
      // $dvd = new Dvd();
      // $furniture = new Furniture();

      if (!empty($_POST)) {

        $request = array($_POST['sku']);
        array_push($request, $_POST['name']);
        array_push($request, $_POST['price']);
        array_push($request, $_POST['productType']);
        array_push($request, empty($_POST['size']) ? NULL : $_POST['size']);
        array_push($request, empty($_POST['weight']) ? NULL : $_POST['weight']);
        array_push($request, empty($_POST['height']) ? NULL : $_POST['height']);
        array_push($request, empty($_POST['width']) ? NULL : $_POST['width']);
        array_push($request, empty($_POST['length']) ? NULL : $_POST['length']);

        $sql = "INSERT INTO products(sku, name, price, productType, size, weight, height, width, length) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $link->prepare($sql);
        $stmt->execute($request);

        header('Location: index.php');
      }

    ?>
    <form class="" id="productsCheck" action="index.html" method="post">
      <div class = "productList">
        <?php
          $query = "SELECT   *  from products";
          $products = $link->prepare($query);
          $products->execute();

          $tableFields = $products->fetchAll(PDO::FETCH_UNIQUE);

          for ($i=1; $i <= count($tableFields); $i++) {

            if ($tableFields[$i]['productType'] == 'dvd') {
                // $productInfo = $dvd->getSizeWeightDimension();
                $productTypeInfo = 'Size: ';
                $productInfo =  $tableFields[$i]['size'] . ' Mb';
            }
            else if ($tableFields[$i]['productType'] == 'book') {
                $productTypeInfo = 'Weight: ';
                $productInfo =  $tableFields[$i]['weight'] . ' Kg';
            }
            else {
                $productTypeInfo = 'Dimensions: ';
                $productInfo = $tableFields[$i]['height'] . 'x' . $tableFields[$i]['width'] . 'x' . $tableFields[$i]['length'] . ' Cm';
            }
            echo '
                  <div class="itemInfo">
                      <div class="checkBoxInfo">
                                <input type="checkbox" class="" id="checkItem"  name="check[]" value="delete">
                      </div>

                      <div class="productInfo">
                        <ul>
                            <li><b>' . $tableFields[$i]['sku'] . '</b></li>
                            <li><b>' . $tableFields[$i]['name'] . '</b></li>
                            <li><b>' . $tableFields[$i]['price'] . ' $ </b></li>
                            <li><b>' . $productTypeInfo . $productInfo . ' </b></li>
                        </ul>
                      </div>
                </div>
            ';
          }

        ?>
      </div>
    </form>


</body>
</html>
