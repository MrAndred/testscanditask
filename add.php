<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "css/add.min.css" rel = "stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gowun+Batang:wght@700&display=swap" rel="stylesheet">
    <title>Add product</title>
</head>
<body>
    <div class = "topMenu">
        <h1>Product Add</h1>
        <div class = "changeList">
                <input type = "submit" value = "Save" class = "saveProduct" form = 'product_form'>
                <input type = "button" value = "Cancel" class = "cancelEdit" >
        </div>
    </div>
    <hr>

    <form action="index.php" id="product_form" class="productsForm" method="post">
    	<div class="formProduct">
    		<label for="sku">SKU </label>
    		<input type="text" name="sku" id="sku" required>
    	</div>

    	<div class="formProduct">
    		<label for="name">Name </label>
    		<input type="text" name="name" id="name"  required>
    	</div>

    	<div class="formProduct">
    		<label for="price">Price </label>
    		<input type="number" name="price" id="price" required>
    	</div>

    	<div class="formProduct">
    		<div class="menu">
    			<label for = "productType">Type Switcher</label>
    			<select id="productType" name="productType">
    				<option value="dvd">DVD</option>
    				<option value="furniture">Furniture</option>
    				<option value="book">Book</option>
    			</select>
    		</div>


    		<div class="content">
    			<div id="dvd" class="data">
    				<label for = "size">Size(MB)</label>
    				<input type="number" name="size" id="size">
    				<p>* Please provide Size in MB</p>
    			</div>
    			<div id="furniture" class="data">
    				<div>
    					<label for = "height">Height(CM)</label>
    					<input type="number" name="height" id="height">
    				</div>
    				<div>
    					<label for = "width">Width(CM)</label>
    					<input type="number" name="width" id="width" >
    				</div>
    				<div>
    					<label for = "length">Length(CM)</label>
    					<input type="number" name="length" id="length">
    				</div>
    				<p>* Please provide dimensions in HxWxL format</p>
    			</div>
    			<div id="book" class="data">
    				<label for = "weight">Weight(KG)</label>
    				<input type="number" name="weight" id="weight">
    				<p>* Please provide Weight in KG</p>
    			</div>
    		</div>
    	</div>
    </form>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
      $(document).ready(function(){
      	$("#productType").on('change', function(){
      		$(".data").hide();
      		$("#" + $(this).val()).fadeIn(300);
      	}).change();
      });
    </script>

    <?php
      require 'footer.php';
    ?>

</body>
</html>
